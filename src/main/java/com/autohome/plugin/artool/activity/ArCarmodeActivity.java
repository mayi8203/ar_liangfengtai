package com.autohome.plugin.artool.activity;

import android.os.Bundle;

import com.autohome.plugin.artool.R;
import com.hiscene.sdk.fragment.BaseARFragment;
import com.hiscene.sdk.utils.Utils;
import com.liulishuo.filedownloader.FileDownloader;
import com.liulishuo.filedownloader.connection.FileDownloadUrlConnection;
import com.liulishuo.filedownloader.services.DownloadMgrInitialParams;

import java.net.Proxy;

/**
 * Created by li on 2017/4/16.
 */

public class ArCarmodeActivity extends BaseActivity {
    private BaseARFragment arFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ar);

        Utils.init(this);
        /**
         * just for cache Application's Context, and ':filedownloader' progress will NOT be launched
         * by below code, so please do not worry about performance.
         * @see FileDownloader#init(Context)
         */
        FileDownloader.init(getApplicationContext(), new DownloadMgrInitialParams.InitCustomMaker()
                .connectionCreator(new FileDownloadUrlConnection
                        .Creator(new FileDownloadUrlConnection.Configuration()
                        .connectTimeout(15_000) // set connection timeout.
                        .readTimeout(15_000) // set read timeout.
                        .proxy(Proxy.NO_PROXY) // set proxy
                )));
//        String path = FileUtil.getDiskCacheDir(this) + File.separator;
//        FileUtil.copyAssetsFolder2SDCard(this, "games", path, true);
//        Global.setResourcePath(path + "demo_car" + File.separator);
        String name;
        if (null == getIntent().getStringExtra("CarName") || "".equals(getIntent().getStringExtra("CarName"))) {
            name = "demo_car";
        } else {
            name = getIntent().getStringExtra("CarName");
        }
        arFragment = new BaseARFragment();
        Bundle bundle =new Bundle();
        bundle.putString("CarName",name);
        arFragment.setArguments(bundle);
        getSupportFragmentManager().beginTransaction().add(R.id.fragment_ar, arFragment).commit();
    }


}
